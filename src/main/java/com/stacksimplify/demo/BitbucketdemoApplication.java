package com.stacksimplify.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BitbucketdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitbucketdemoApplication.class, args);
	}

}
